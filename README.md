# Lab5 -- Integration testing

## Bugs

- Wrong calculation for fixed_price, budget type 
- Wrong calculation for minute, luxury type
- Distance, planed distance, time, planed time should accept only numbers greater than zero.
- Fixed price plan should not be allowed for luxury type.
- Nonsense parameter passes for plan

## BVA 

Type: "budget", "luxury", "nonsense"

Plan: "minute", "fixed_price", "nonsense"

Distance: "-10", "0", "10", "1000", "nonsense"

Planed Distance: "-10", "0", "10", "1000", "nonsense"

Time: "-10", "0", "10", "1000", "nonsense"

Planed time: "-10", "0", "10", "1000", "nonsense"

InnoDiscount: "yes", "no", "nonsense"


## Rates

fixed price = 11

budget / min = 17

luxury / min = 33



## Request Table
| type | plan | distance | planed distance | time | planed time | inno discount | expected | actual |
| - | - | - | - | - | - | - | - | - |
| budget | minute | 10 | 10 | 10 | 10 | yes | 144 | {"price" : 144} | 
| budget | minute | 10 | 10 | 10 | 10 | no | 150 | {"price" : 150} | 
| budget | minute | 10 | 10 | 11 | 10 | no | 150 | {"price" : 165} | 
| budget | minute | 10 | 10 | 09 | 10 | no | 150 | {"price" : 165} | 
| luxury | fixed_price | 10 | 10 | 10 | 10 | no | Invalid request | {"price" : 125} | 
| luxury | fixed_price | -1 | 10 | 10 | 10 | no | Invalid request | {"price" : 166.66666666666666} | 
| luxury | nonsense | -1 | 10 | 10 | 10 | no | Invalid request | {"price" : 166.66666666666666} | 
| luxury | nonsense | -1 | 10 | -1 | 10 | no | Invalid request | {"price" : 166.66666666666666} | 
| budget | minute | 10 | 10 | 0 | 10 | no | Invalid request | {"price" : 0} | 
| budget | fixed_price | 10 | 10 | 10 | 10 | no | 110 | {"price" : 150} | 
| budget | fixed_price | 10 | 10 | 10 | 10 | nonsense | Invalid request | Invalid request | 



## Bug test cases
| type | plan | distance | planed distance | time | planed time | inno discount | expected | actual |
| - | - | - | - | - | - | - | - | - |
| budget | fixed_price | 10 | 10 | 10 | 10 | no | 110 | {"price" : 150} | 
| budget | fixed_price | -1 | 10 | 10 | 10 | no | Invalid request | {"price" : 166.66666666666666} | 
| luxury | minute | 10 | 10 | 10 | 10 | no | 330 | {"price" : 370} | 
| luxury | fixed_price | 10 | 10 | 10 | 10 | no | Invalid request | {"price" : 125} | 
| luxury | fixed_price | -1 | 10 | 10 | 10 | no | Invalid request | {"price" : 166.66666666666666} |
| luxury | nonsense | -1 | 10 | -1 | 10 | no | Invalid request | {"price" : 166.66666666666666} | 



